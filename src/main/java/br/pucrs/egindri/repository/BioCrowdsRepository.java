package br.pucrs.egindri.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.pucrs.egindri.model.WorldRequest;

@Repository
public interface BioCrowdsRepository extends MongoRepository<WorldRequest, String> {}
