package br.pucrs.egindri.response;

import java.util.List;

import br.pucrs.egindri.core.Vector3;

public class SimulationResponse {

	public List<List<Vector3>> positions;

	
	public SimulationResponse(List<List<Vector3>> positions) {
		this.positions = positions;
	}
	
	
	public List<List<Vector3>> getPositions() {
		return positions;
	}
}
