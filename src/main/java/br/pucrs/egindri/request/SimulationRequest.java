package br.pucrs.egindri.request;

import br.pucrs.egindri.model.WorldRequest;

public class SimulationRequest {

	private WorldRequest worldRequest;
	
	private int numberOfFrames;
	
	
	public WorldRequest getWorldRequest() {
		return worldRequest;
	}
	
	public int getNumberOfFrames() {
		return numberOfFrames;
	}
}
