package br.pucrs.egindri.core;

import java.util.ArrayList;
import java.util.List;

public class Cell extends SimulationObject {

	private int x;
    private int y;
    
    private String name;

    private List<Auxin> auxins = new ArrayList<Auxin>();

	
    public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public List<Auxin> getAuxins() {
		return auxins;
	}

	public String getName() {
		return name;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuxins(List<Auxin> auxins) {
		this.auxins = auxins;
	}
}
