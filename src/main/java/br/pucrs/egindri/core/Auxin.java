package br.pucrs.egindri.core;

public class Auxin {

	private boolean isTaken = false;

    private Vector3 position;
    
    private double minDistance = 2;
    
    private Agent agent;

    private Cell cell;
    
    private String name;
    
    
    public Auxin(String name, Cell cell, Vector3 position) {
    	this.name = name;
    	this.cell = cell;
    	this.position = position;
    }


	public void resetAuxin() {
        minDistance = 2;
        agent = null;
        isTaken = false;
    }


	public boolean isTaken() {
		return isTaken;
	}

	public Vector3 getPosition() {
		return position;
	}

	public double getMinDistance() {
		return minDistance;
	}

	public Agent getAgent() {
		return agent;
	}

	public Cell getCell() {
		return cell;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public void setTaken(boolean isTaken) {
		this.isTaken = isTaken;
	}


	public void setPosition(Vector3 position) {
		this.position = position;
	}


	public void setMinDistance(double distanceSqr) {
		this.minDistance = distanceSqr;
	}


	public void setAgent(Agent agent) {
		this.agent = agent;
	}


	public void setCell(Cell cell) {
		this.cell = cell;
	}
}
