package br.pucrs.egindri.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class World {
	
	private static final double AGENT_RADIUS = 1;
	private static final double AUXIN_RADIUS = 0.1f;
	private static final double AUXIN_DENSITY = 0.45f;

	private SimulationObject goal = new SimulationObject();
	private Vector2 dimension = new Vector2(400, 200);

	private List<Agent> agents = new ArrayList<>();
	private List<Cell> cells = new ArrayList<>();

	private int maxAuxins;
	private boolean isReady;

    Map<Vector3, Boolean> map;
    
    
    public World(List<Vector3> agentInitialPositions, Vector3 goal) {
    	map = new HashMap<>();
    	
    	for (int i = 0; i < dimension.getX() / 2; i++) {
    		for (int j = 0; j < dimension.getY() / 2; j++) {
    			map.put(new Vector3(i, j, 0), true);
    		}
    	}
    	
    	this.goal.setPosition(goal);
    	
    	createCells();
    	dartThrowing();
    	createAgents(agentInitialPositions);
    	
    	isReady = true;
    }
	
	private void createCells() {

		for (int i = 0; i < dimension.getX() / 2; i++) {
			for (int j = 0; j < dimension.getY() / 2; j++) {
				Cell newCell = new Cell();

				newCell.setName("Cell [" + i + "][" + j + "]");

				newCell.setX(i);
				newCell.setY(j);
				newCell.setPosition(new Vector3(i, j, 0));

				cells.add(newCell);
			}
		}
	}

	private void dartThrowing() {
		double densityToQuantity = AUXIN_DENSITY;

		densityToQuantity *= 2f / (2 * AUXIN_RADIUS);
		densityToQuantity *= 2f / (2 * AUXIN_RADIUS);

		maxAuxins = (int) Math.floor(densityToQuantity);

		for (int c = 0; c < cells.size(); c++) {
			int flag = 0;
			for (int i = 0; i < maxAuxins; i++) {
				double x = ThreadLocalRandom.current().nextDouble(cells.get(c).getPosition().getX() - 0.99d, cells.get(c).getPosition().getX() + 0.99d);// - cells.get(c).getPosition().getX() - 0.99d) *  new Random().nextDouble();
				double y = ThreadLocalRandom.current().nextDouble(cells.get(c).getPosition().getY() - 0.99d, cells.get(c).getPosition().getY() + 0.99d);// - cells.get(c).getPosition().getZ() - 0.99d) *  new Random().nextDouble();

				List<Auxin> allAuxinsInCell = cells.get(c).getAuxins();
				boolean createAuxin = true;
				for (int j = 0; j < allAuxinsInCell.size(); j++) {
					double distanceAASqr = (new Vector3(x, y, 0).subtract(allAuxinsInCell.get(j).getPosition()))
							.squaredMagnitude();

					if (distanceAASqr < AUXIN_RADIUS * AUXIN_RADIUS) {
						createAuxin = false;
						break;
					}
				}

				if (createAuxin) {
					createAuxin = map.get(new Vector3((int) x, (int) y, 0));
				}

				if (createAuxin) {
					Auxin newAuxin = new Auxin("Auxin [" + c + "][" + i + "]", cells.get(c), new Vector3(x, y, 0));

					cells.get(c).getAuxins().add(newAuxin);

					flag = 0;

					if (i % 200 == 0) {
						//return;
					}

				} else {
					flag++;
					i--;
				}

				if (flag > maxAuxins * 2) {
					flag = 0;
					break;
				}
			}
		}
	}

	private void createAgents(List<Vector3> agentInitialPositions) {
		for (Vector3 position : agentInitialPositions) {
			Agent newAgent = new Agent();

			newAgent.setCurrentCell(cells.stream().filter(c -> c.getPosition().equals(position)).findAny().get());
			newAgent.setAgentRadius(AGENT_RADIUS);
			newAgent.setGoal(goal);
			newAgent.setWorld(this);

			newAgent.setPosition(position);
			agents.add(newAgent);
			newAgent.start();
		}
	}

	public void update() {
		if (!isReady) {
			return;
		}

		for (int i = 0; i < cells.size(); i++) {
			for (int j = 0; j < cells.get(i).getAuxins().size(); j++) {
				cells.get(i).getAuxins().get(j).resetAuxin();
			}
		}

		for (int i = 0; i < agents.size(); i++) {
			agents.get(i).findNearAuxins();
		}

		for (int i = 0; i < agents.size(); i++) {
			List<Auxin> agentAuxins = agents.get(i).getAuxins();

			for (int j = 0; j < agentAuxins.size(); j++) {
				agents.get(i).distAuxin.add(agentAuxins.get(j).getPosition().subtract(agents.get(i).getPosition()));
			}

			agents.get(i).calculateDirection();
			agents.get(i).calculateVelocity();
			agents.get(i).step();
			agents.get(i).update();
		}
	}

	public List<Cell> getCells() {
		return cells;
	}

	public Vector2 getDimension() {
		return dimension;
	}
	
	public Map<Vector3, Boolean> getMap() {
		return map;
	}
	
	public List<Agent> getAgents() {
		return agents;
	}
}