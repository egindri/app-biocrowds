package br.pucrs.egindri.core;

public class Vector3 {

	public static final Vector3 ZERO = new Vector3(0, 0, 0);
	
	private double x;
	private double y;
	private double z;
	
	
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}


	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}
	
	public Vector3 add(Vector3 vector) {
		
		return new Vector3(x + vector.x, y + vector.y, z + vector.z);
	}

	public Vector3 subtract(Vector3 vector) {
		
		return new Vector3(x - vector.x, y - vector.y, z - vector.z);
	}
	
	public Vector3 multiply(double factor) {
		return new Vector3(x * factor, y * factor, z * factor);
	}

	public Vector3 multiply(Vector3 vector) {
		return new Vector3(x * vector.getX(), y * vector.getY(), z * vector.getZ());
	}

	public Vector3 divide(double moduleM) {
		return new Vector3(x / moduleM, y / moduleM, z / moduleM);
	}
	
	public double squaredMagnitude() {
		return x * x + y * y + z * z;
	}

	public double magnitude() {
		return Math.sqrt(squaredMagnitude());
	}
	
	public Vector3 normalized() {
		double squaredMagnitude = squaredMagnitude();
		
		if (squaredMagnitude == 0) {
			return new Vector3(0, 0, 0);
		}
		
		return new Vector3(x / squaredMagnitude, y / squaredMagnitude, z / squaredMagnitude);
	}
	
	public static double distance(Vector3 a, Vector3 b) {
		return a.subtract(b).magnitude();
	}
	
	public static double dot(Vector3 a, Vector3 b) {
		return a.getX() * b.getX() + a.getY() * b.getY() + a.getZ() * b.getZ();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector3 other = (Vector3) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}
}