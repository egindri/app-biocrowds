package br.pucrs.egindri.core;

public class SimulationObject {
	
	protected Vector3 position;
	
	
	public Vector3 getPosition() {
		return position;
	}
	
	public void setPosition(Vector3 position) {
		this.position = position;
	}
}
