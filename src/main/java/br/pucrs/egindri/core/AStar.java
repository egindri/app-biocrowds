package br.pucrs.egindri.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AStar {
	
	private World world;
	private Agent agent;
	
	
	public AStar(World world, Agent agent) {
		this.world = world;
		this.agent = agent;
	}

	public double euclidianDistance(Vector3 position, Vector3 goal) {
		return Vector3.distance(position, goal);
	}
	
	public List<Vector3> obtainNearbyPositions(Vector3 position) {
		List<Vector3> positions = new ArrayList<>();
		
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				Vector3 newPosition = new Vector3(Math.round(position.getX()), Math.round(position.getY()), Math.round(position.getZ())).add(new Vector3(i, j, 0));
				
				if (world.getMap().getOrDefault(newPosition, false) //!a.equals(agent) && 
				&& !world.getAgents().stream().anyMatch(a -> !a.equals(agent) && new Vector3(Math.round(a.getPosition().getX()), Math.round(a.getPosition().getY()), Math.round(a.getPosition().getZ())).equals(newPosition))) {
					positions.add(newPosition);
				}
			}
		}
		
		return positions;
	}
	
	public List<Vector3> solve(Vector3 state, Vector3 goal) {
		Map<Vector3, Double> preceding_costs = new HashMap<>(Map.of(state, 0d));
		Map<Vector3, Double> estimated_future_costs = new HashMap<>(Map.of(state, euclidianDistance(state, goal))); 
		Map<Vector3, Vector3> state_action_predecessors = new HashMap<>();
		
		Set<Vector3> open_states = new HashSet<>(Set.of(state));
		Set<Vector3> closed_states = new HashSet<>();
		Vector3 lowestEstimatedDistanceToEnd = null;
		Double lowestEstimatedCost = Double.MAX_VALUE;
				
		while (open_states.size() > 0) {
			
			Vector3 current_state = null;
			Double current_estimated_cost = null;
					
			for (Vector3 s : open_states) {
				
				if (current_state == null || estimated_future_costs.get(s) < current_estimated_cost) {
					current_estimated_cost = estimated_future_costs.get(s);
					current_state = s;
				}
			}
			
			if (current_state.equals(goal)) {
				List<Vector3> plan = new ArrayList<>();
				while (state_action_predecessors.containsKey(current_state)) {
					
					current_state = state_action_predecessors.get(current_state);
					plan.add(current_state);
				}
				Collections.reverse(plan);
				plan.remove(0);
				return plan;
			}
											
			open_states.remove(current_state);
			closed_states.add(current_state);
											
			for (Vector3 candidate_state : obtainNearbyPositions(current_state)) {
				if (!closed_states.contains(candidate_state)) {
					double candidate_cost = preceding_costs.get(current_state) + 1;
					if (!preceding_costs.containsKey(candidate_state) || candidate_cost < preceding_costs.get(candidate_state)) {
						open_states.add(candidate_state);
						state_action_predecessors.put(candidate_state, current_state);
						preceding_costs.put(candidate_state, candidate_cost);
						double estimated_candidate_cost = preceding_costs.get(candidate_state) + euclidianDistance(candidate_state, goal);
						estimated_future_costs.put(candidate_state, estimated_candidate_cost);
						
						if (estimated_candidate_cost < lowestEstimatedCost) {
							lowestEstimatedDistanceToEnd = candidate_state;
							lowestEstimatedCost = estimated_candidate_cost;
						}
					}
				}
			}
		}
		
		List<Vector3> plan = new ArrayList<>();
		
		while (state_action_predecessors.containsKey(lowestEstimatedDistanceToEnd)) {
			
			lowestEstimatedDistanceToEnd = state_action_predecessors.get(lowestEstimatedDistanceToEnd);
			plan.add(lowestEstimatedDistanceToEnd);
		}
		Collections.reverse(plan);
		plan.remove(0);
		return plan;
	}
}
