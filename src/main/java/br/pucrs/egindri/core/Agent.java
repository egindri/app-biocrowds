package br.pucrs.egindri.core;

import java.util.ArrayList;
import java.util.List;

public class Agent extends SimulationObject {
	
	private static final double UPDATE_NAVMESH_INTERVAL = 1;

    public double agentRadius;
    public Vector3 velocity;
    private double maxSpeed = 1.5f;

    public SimulationObject goal;

    private List<Auxin> auxins = new ArrayList<Auxin>();

    private Cell currentCell;

    private World world;

    private int totalX;
    private int totalY;

    private double elapsedTime;
    public List<Vector3> distAuxin = new ArrayList<>();

    private boolean isDenW = false;
    private double denW;
    private Vector3 rotation = new Vector3(0, 0, 0);
    private Vector3 goalPosition = new Vector3(0, 0, 0);
    private Vector3 dirAgentGoal = new Vector3(0, 0, 0);
    
    private String name;
      

    void start() {

        goalPosition = goal.getPosition();
        dirAgentGoal = goalPosition.subtract(position);

        totalX = (int) Math.floor(world.getDimension().getX() / 2.0f) - 1;
        totalY = (int) Math.floor(world.getDimension().getY() / 2.0f);
    }

    void update() {
        clearAgent();
        
        List<Vector3> foundPath = new AStar(world, this).solve(position, goal.getPosition());

        if (!foundPath.isEmpty()) {
        	goalPosition = foundPath.get(0);
        	dirAgentGoal = goalPosition.subtract(new Vector3(Math.round(position.getX()), Math.round(position.getY()), Math.round(position.getZ())));
        } else {
        	goalPosition = position;
        	dirAgentGoal = new Vector3(0,0,0);
        }
    }

    void clearAgent() {
        denW = 0;
        distAuxin.clear();
        isDenW = false;
        rotation = new Vector3(0, 0, 0);
        dirAgentGoal = goalPosition.subtract(position);
    }

    public void step() {
        if (velocity.squaredMagnitude() > 0) {
        	position = position.add(velocity);
        	//transform.Translate(velocity * Time.deltaTime, Space.World);
        }
    }

    public void calculateDirection() {
        for (int k = 0; k < distAuxin.size(); k++) {
            double valorW = calculaW(k);
            if (denW < 0.0001f) {
            	valorW = 0;
            }

            rotation = rotation.add(distAuxin.get(k).multiply(valorW  * maxSpeed));
        }
    }

    double calculaW(int indiceRelacao) {
        double fVal = getF(indiceRelacao);

        if (!isDenW) {
            denW = 0f;

            for (int k = 0; k < distAuxin.size(); k++) {
                denW += getF(k);
            }
            isDenW = true;
        }

        return fVal / denW;
    }

    double getF(int pRelationIndex) {
        double Ymodule = Vector3.distance(distAuxin.get(pRelationIndex), Vector3.ZERO);
        double Xmodule = dirAgentGoal.normalized().magnitude();

        double dot = Vector3.dot(distAuxin.get(pRelationIndex), dirAgentGoal.normalized());

        if (Ymodule < 0.00001f || Xmodule < 0.00001f) { //XModule?
        	return 0;
        }

        return (double)((1.0 / (1.0 + Ymodule)) * (1.0 + ((dot) / (Xmodule * Ymodule))));
    }

    public void calculateVelocity() {
        double moduleM = Vector3.distance(rotation, Vector3.ZERO);

        double s = moduleM * (double) Math.PI;

        if (s > maxSpeed) {
        	s = maxSpeed;
        }

        if (moduleM > 0.0001f) {
            velocity = rotation.divide(moduleM).multiply(s);
        } else {
            velocity = Vector3.ZERO;
        }
    }

    public void findNearAuxins() {
        auxins.clear();

        List<Auxin> cellAuxins = currentCell.getAuxins();

        for (int i = 0; i < cellAuxins.size(); i++) {
            double distanceSqr = (position.subtract(cellAuxins.get(i).getPosition())).squaredMagnitude();
            if (distanceSqr < cellAuxins.get(i).getMinDistance() && distanceSqr <= agentRadius * agentRadius) {
                if (cellAuxins.get(i).isTaken()) {
                	cellAuxins.get(i).getAgent().getAuxins().remove(cellAuxins.get(i));
                }

                cellAuxins.get(i).setTaken(true);

                cellAuxins.get(i).setAgent(this);
                cellAuxins.get(i).setMinDistance(distanceSqr);
                auxins.add(cellAuxins.get(i));
            }
        }

        findCell();
    }

    private void findCell() {
        Double distanceToCellSqr = (position.subtract(currentCell.getPosition())).squaredMagnitude();

        if (currentCell.getX() > 0) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() - 1) * totalY + (currentCell.getY() + 0)));
        }

        if (currentCell.getX() > 0 && currentCell.getY() < totalY - 1) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() - 1) * totalY + (currentCell.getY() + 1)));
        }

        if (currentCell.getX() > 0 && currentCell.getY() > 0) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() - 1) * totalY + (currentCell.getY() - 1)));
        }

        if (currentCell.getY() < totalY - 1) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() + 0) * totalY + (currentCell.getY() + 1)));
        }

        if (currentCell.getX() < totalX && currentCell.getY() < totalY - 1) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() + 1) * totalY + (currentCell.getY() + 1)));
        }

        if (currentCell.getX() < totalX) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() + 1) * totalY + (currentCell.getY() + 0)));
        }

        if (currentCell.getX() < totalX && currentCell.getY() > 0) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() + 1) * totalY + (currentCell.getY() - 1)));
        }

        if (currentCell.getY() > 0) {
        	distanceToCellSqr = checkAuxins(distanceToCellSqr, world.getCells().get((currentCell.getX() + 0) * totalY + (currentCell.getY() - 1)));
        }
    }

    private Double checkAuxins(Double pDistToCellSqr, Cell pCell) {
        List<Auxin> cellAuxins = pCell.getAuxins();

        for (int c = 0; c < cellAuxins.size(); c++) {
            Double distanceSqr = (position.subtract(cellAuxins.get(c).getPosition())).squaredMagnitude();
            if (distanceSqr < cellAuxins.get(c).getMinDistance() && distanceSqr <= agentRadius * agentRadius) {
                if (cellAuxins.get(c).isTaken()) {
                	cellAuxins.get(c).getAgent().getAuxins().remove(cellAuxins.get(c));
                }

                cellAuxins.get(c).setTaken(true);
                cellAuxins.get(c).setAgent(this);
                cellAuxins.get(c).setMinDistance(distanceSqr);
                auxins.add(cellAuxins.get(c));
            }
        }

        Double distanceToNeighbourCell = (position.subtract(pCell.getPosition())).squaredMagnitude(); 
        if (distanceToNeighbourCell < pDistToCellSqr) {
        	currentCell = pCell;
            return distanceToNeighbourCell;
        }
        
        return pDistToCellSqr;
    }
    
    public List<Auxin> getAuxins() {
    	return auxins;
    }
    
    public void setName(String name) {
    	this.name = name;
    }

    public void setCurrentCell(Cell currentCell) {
    	this.currentCell = currentCell;
    }

    public void setAgentRadius(double agentRadius) {
    	this.agentRadius = agentRadius;
    }

    public void setGoal(SimulationObject goal) {
    	this.goal = goal;
    }

    public void setWorld(World world) {
    	this.world = world;
    }
    
    public String getName() {
    	return name;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
