package br.pucrs.egindri.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.pucrs.egindri.core.Vector3;
import br.pucrs.egindri.core.World;
import br.pucrs.egindri.model.WorldRequest;
import br.pucrs.egindri.repository.BioCrowdsRepository;

@Service
public class BioCrowdsService {

	@Autowired
	private BioCrowdsRepository repository;
	
	
	public String save(WorldRequest document) {
		return repository.save(document).getId();
	}
	
	public List<List<Vector3>> simulate(WorldRequest request, Integer numberOfFrames) {
		
		List<List<Vector3>> positionsMap = new ArrayList<>();
		
		World world = new World(request.getAgentInitialPositions(), request.getGoal());
		
		for (int i = 0; i < numberOfFrames; i++) {
			List<Vector3> positions = world.getAgents().stream().map(a -> a.getPosition()).collect(Collectors.toList());
			world.update();
			positionsMap.add(positions);
		}
		
		return positionsMap;
	}
}