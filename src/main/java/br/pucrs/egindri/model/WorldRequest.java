package br.pucrs.egindri.model;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.pucrs.egindri.core.Vector3;

@Document
public class WorldRequest {

	@Id
	private String id;
	
	@NotEmpty
	@NotNull
	private List<Vector3> agentInitialPositions;
	
	@NotNull
	private Vector3 dimensions;
	
	@NotNull
	private Vector3 goal;
	
	
	public String getId() {
		return id;
	}
	
	public List<Vector3> getAgentInitialPositions() {
		return agentInitialPositions;
	}
	
	public Vector3 getDimensions() {
		return dimensions;
	}
	
	public Vector3 getGoal() {
		return this.goal;
	}
}
