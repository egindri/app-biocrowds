package br.pucrs.egindri.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.pucrs.egindri.model.WorldRequest;
import br.pucrs.egindri.request.SimulationRequest;
import br.pucrs.egindri.response.SimulationResponse;
import br.pucrs.egindri.service.BioCrowdsService;


@CrossOrigin(origins = "*")
@RequestMapping("world")
@RestController
public class BioCrowdsController {

	@Autowired
	private BioCrowdsService service;


	@GetMapping("{id}")
	public SimulationResponse getSimulation(@PathVariable String id) {
		return null;
	}
	
	@PostMapping
	public String postSimulation(SimulationRequest request) {
		return service.save(request.getWorldRequest());
	}
	
	@PostMapping("/simulation")
	public SimulationResponse simulation(@RequestBody WorldRequest request, @RequestParam Integer numberOfFrames) {
		return new SimulationResponse(service.simulate(request, numberOfFrames));
	}
}