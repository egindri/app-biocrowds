package br.pucrs.egindri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppBioCrowds {

	public static void main(String[] args) {
		SpringApplication.run(AppBioCrowds.class, args);
	}
}